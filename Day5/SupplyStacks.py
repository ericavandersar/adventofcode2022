import re
import copy


def get_stacks(lines):
    input_stacks = lines[0].split("\n")
    nr_stacks = input_stacks[-1].strip("   ").split("   ")
    my_stacks = [
        [
            line[input_stacks[-1].find(stack)]
            for line in input_stacks[:-1]
            if (len(line) > input_stacks[-1].find(stack))
        ] for stack in nr_stacks]

    my_stacks = [[x for x in stack if x != ' '] for stack in my_stacks]
    [stack.reverse() for stack in my_stacks]
    return my_stacks


def get_actions(lines):
    input_actions = lines[1].split("\n")
    return [list(map(int, re.findall(r'\d+', action))) for action in input_actions]


def move_crates(stacks, actions, part1 = True):
    for action in actions:
        # print("Current action: ", action)
        to_move = stacks[action[1]-1][-action[0]:]
        # print("items of stack to be moved ", to_move)
        # add to second stack
        if part1:
            to_move = reversed(to_move)
        stacks[action[2] - 1].extend(to_move)
        # print("stack after addition: ", stacks[action[2] - 1])
        # delete from first stack
        del(stacks[action[1] - 1][-action[0]:])
        # print("stack after removal: ", stacks[action[1]-1])
    return stacks


if __name__ == "__main__":
    file_name = 'data.txt'

    lines = (open(file_name).read()).split("\n\n")
    stacks = get_stacks(lines)
    actions = get_actions(lines)

    # Part 1
    stacks_1 = move_crates(copy.deepcopy(stacks), actions)
    print("Top of stack is: ", ''.join(map(str, [stack[-1] for stack in stacks_1])))

    # Part 1
    stacks_2 = move_crates(copy.deepcopy(stacks), actions, part1=False)
    print("Top of stack is: ", ''.join(map(str, [stack[-1] for stack in stacks_2])))

