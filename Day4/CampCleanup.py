file_name = 'data.txt'

lines = (open(file_name).read()).split("\n")

# Part 1
count = 0

for line in lines:
    elf_1, elf_2 = line.split(",")
    start_elf_1, end_elf_1 = [int(x) for x in elf_1.split("-")]
    start_elf_2, end_elf_2 = [int(x) for x in elf_2.split("-")]
    if (start_elf_1 <= start_elf_2) & (end_elf_1 >= end_elf_2):
        # print("elf 1 contains elf 2", line)
        count += 1
    elif (start_elf_1 >= start_elf_2) & (end_elf_1 <= end_elf_2):
        # print("elf 2 contains elf 1", line)
        count += 1

print("Asnwer part 1: ", count)

# Part 2
count = 0

for line in lines:
    elf_1, elf_2 = line.split(",")
    start_elf_1, end_elf_1 = [int(x) for x in elf_1.split("-")]
    start_elf_2, end_elf_2 = [int(x) for x in elf_2.split("-")]
    if (start_elf_1 <= start_elf_2) & (end_elf_1 >= start_elf_2):
        count += 1
    elif (start_elf_1 >= start_elf_2) & (start_elf_1 <= end_elf_2):
        count += 1

print("Asnwer part 2: ", count)
