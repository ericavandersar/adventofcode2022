
class FileTree:
    def __init__(self, name: str):
        self.name = name
        self.size = 0
        self.subfolders = []
        self.tot_size = 0

    def add_folders(self, folder):
        self.subfolders.append(folder)
        self.size += folder.size

    def __repr__(self):
        return f"%s: %d" % (self.name, self.size)


def folder_list(lines):
    current_path = []
    folder_list = []
    for line in lines:
        if line.startswith("$ cd "):
            folder_name: str = line.split()[2]
            if folder_name != "..":
                new_folder = FileTree(folder_name)
                current_path.append(new_folder)
                folder_list.append(new_folder)
            else:
                # step back to previous folder
                size_sub_folder = current_path[-1].size
                current_path.pop()
                current_path[-1].size += size_sub_folder
        elif not (line.startswith("$") | line.startswith("dir")):
            size_file = int(line.split()[0])
            current_path[-1].size += size_file
    while len(current_path) > 0:
        # step back to previous folder
        size_sub_folder = current_path[-1].size
        current_path.pop()
        if len(current_path):
            current_path[-1].size += size_sub_folder

    return folder_list


def sum_small_dirs(directories, size = 100000):
    sum_dir = 0
    for dir in directories:
        if dir.size < size:
            sum_dir += dir.size
    return sum_dir


def folders_to_delete(directories, space=70000000, needed=30000000):
    avail = space - directories[0].size
    needed = needed - avail
    print("We still need to free %d space" % needed)
    smallest = directories[0].size
    name_smallest = ''
    for d in directories:
        if (d.size >= needed) & (d.size < smallest):
            smallest = d.size
            name_smallest = d.name
    return smallest, name_smallest


if __name__ == "__main__":
    file_name = 'input.txt'
    # file_name = 'test_data.txt'
    lines = open(file_name).readlines()
    folders = folder_list(lines)

    # part 1
    print("Sum of all small directories is ", sum_small_dirs(folders))

    # part 2
    print(f"Size folder to delete %d with name %s" % folders_to_delete(folders))


