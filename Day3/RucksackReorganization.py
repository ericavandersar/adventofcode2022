import string

file_name = "data.txt"

lines = (open(file_name).read()).split("\n")
values = dict(zip(string.ascii_letters, range(1, 53)))

# part 1
tot = 0
for line in lines:
    mid = len(line)//2  # use double // to get integer value
    comp1, comp2 = line[:mid], line[mid:]
    for char in comp1:
        if char in comp2:
            break
    tot += values[char]
print("Result part 1: ", tot)

# part 2
tot = 0
nr_elfs = len(lines)
for elf in range(0, nr_elfs, 3):
    for char in lines[elf]:
        if (char in lines[elf+1]) & (char in lines[elf+2]):
            break
    tot += values[char]
print("Result part 2: ", tot)

