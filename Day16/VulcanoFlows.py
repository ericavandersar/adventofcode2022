import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import copy

class VulcanoEnv:
    def __init__(self, conn_matrix: np.array([[]]), rates, max_time = 30):
        self.conn_matrix: np.array([[]]) = conn_matrix
        self.rates: np.array([]) = np.array(rates)
        self.n_actions = len(rates)
        # number of states is +1 due to time
        self.n_states = len(rates) +1
        # initialize environment
        self.state = 0
        self.time_left = max_time
        self.max_time = max_time
        self.opened = np.zeros(len(rates))

    def reset(self):
        self.state = 0
        self.time_left = self.max_time
        self.opened = np.zeros(len(rates))

    def get_actions(self):
        return self.conn_matrix[self.state].nonzero()

    def step(self, action):
        reward = self.rates * self.opened
        if self.state == action:
            self.opened[action] = 1
        self.time_left -= 1
        self.state = action
        return self.state, reward


def node_by_node(env):
    t = 30


def create_graph(lines):
    lines.sort()
    nodes = []
    rates = []
    connections = []
    for line in lines:
        part1, part2 = line.split(';')
        part1 = part1.split()
        nodes.append(part1[1])
        rates.append(int(part1[4].split('=')[1]))
        if len(part2) == 25:
            # only 1 connection
            connections.append([part2[23:]])
        else:
            # multiple connections
            connections.append(part2[24:].split(', '))

    print(connections)

    # create connectivity  matrix
    n_nodes = len(nodes)
    matrix = np.eye(n_nodes) # * rates
    for i, node in enumerate(nodes):
        for connection in connections[i]:
            matrix[i, nodes.index(connection)] = 1

    return np.array(rates), matrix, nodes


def solution(t, pos, to_release, distances):
    actions = to_release.nonzero()[0]
    best = 0
    print(t)
    for act in actions:
        t_left = t - distances[pos, act]
        if t_left >= 0:
            sol_value = t * to_release[act]
            next_release = to_release.copy()
            next_release[act] = 0
            best = max(best, sol_value + solution(t_left, act, next_release, distances))

    return best


# return shortest paths between each pair of nodes.
def shortest_paths(conn_matrix : np.array([[]])):
    d = np.inf * np.ones(conn_matrix.shape)
    d[conn_matrix.nonzero()] = 1
    for k in range(len(d)):
        for i in range(len(d)):
            for j in range(len(d)):
                if d[i,k] + d[k,j] < d[i,j]:
                    d[i,j] = d[i,k] + d[k,j]
    return d


if __name__ == "__main__":
    # file_name = 'input.txt'
    file_name = 'test_input.txt'
    lines = open(file_name).read().split('\n')
    rates, matrix, nodes = create_graph(lines)
    print(rates)
    print(matrix)
    distances = shortest_paths(matrix)
    print(distances)
    sol = solution(30, 0, rates, distances)
    print(sol)

    ####  CREATE A GRAPH ####

    # node_rate_list = [node + ": " + str(rate) for node, rate in zip(nodes, rates)]
    # mapping = dict([(i, node) for i,node in enumerate(node_rate_list)])
    # plt.figure(figsize=(20,20))
    # G = nx.from_numpy_matrix(matrix)  # np.matrix([[0,1],[1,0]]))
    # G = nx.relabel_nodes(G, mapping)
    # pos = nx.spring_layout(G)
    # nx.draw(G, pos, edge_color='black', width=1, linewidths=1,
    # node_size=1000, with_labels=True, font_color="whitesmoke", font_size=10)
    #
    # ax = plt.gca()
    # ax.margins(0.08)
    # plt.axis("off")
    # plt.savefig("Real_VulcanoNetwork.png")
