import numpy as np

file_name = 'data.txt'
# Part 1
max_cal = 0
current_cal = 0
lines = []

# Part 2
top_3 = np.zeros(3)

with open(file_name) as f:
    lines = (f.read()).split("\n\n")

for line in lines:
    # print("Here is a new Elf:\n", line)
    current_cal = np.sum([np.fromstring(line, dtype=int, sep="\n")])
    if current_cal > max_cal:
        max_cal = current_cal
    if current_cal > np.min(top_3):
        top_3[np.argmin(top_3)] = current_cal

print("Max calories: ", max_cal)
print("Top 3: ", top_3)
print("Sum top three: ", np.sum(top_3))

