import numpy as np

file_name = 'test_data.txt'
# Part 1
score = 0

lines = (open(file_name).read()).split("\n")
for line in lines:
    if line.endswith("X"):
        score += 1
        if line.startswith("C"):
            score += 6
        elif line.startswith("A"):
            score += 3
    elif line.endswith("Y"):
        score += 2
        if line.startswith("A"):
            score += 6
        elif line.startswith("B"):
            score += 3
    elif line.endswith("Z"):
        score += 3
        if line.startswith("B"):
            score += 6
        elif line.startswith("C"):
            score += 3

print("Final score part 1", score)

# part 2
score = 0
for line in lines:
    if line.endswith("X"):
        if line.startswith("A"):
            score += 3
        elif line.startswith("B"):
            score += 1
        elif line.startswith("C"):
            score += 2
    elif line.endswith("Y"):
        score += 3
        if line.startswith("A"):
            score += 1
        elif line.startswith("B"):
            score += 2
        elif line.startswith("C"):
            score += 3
    elif line.endswith("Z"):
        score += 6
        if line.startswith("A"):
            score += 2
        if line.startswith("B"):
            score += 3
        elif line.startswith("C"):
            score += 1

print("Final score part 2", score)