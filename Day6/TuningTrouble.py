file_name = 'data.txt'


def is_unique(string):
    for char in string:
        pos = (string.find(char, string.find(char) + 1))
        if pos > -1:
            return string.find(char) + 1
    return pos

input = open(file_name).read()

done = False

# Part 1
fantastic4 = input[:4]

i = 4
pos = is_unique(fantastic4)
while pos > -1:
    # print(fantastic4, ' ', i, pos)
    fantastic4 = fantastic4[pos:] + input[i:i+pos]
    i += pos
    pos = is_unique(fantastic4)

print(fantastic4, ' ', i, pos)

#Part 2
fantastic14 = input[:14]
i = 14
pos = is_unique(fantastic14)
while pos > -1:
    fantastic14 = fantastic14[pos:] + input[i:i+pos]
    i += pos
    pos = is_unique(fantastic14)

print(fantastic14, ' ', i, pos)