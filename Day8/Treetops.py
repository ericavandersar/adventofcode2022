import numpy as np


def tree_val(x, y, trees):
    # initialize visible trees
    vis_up = 0
    vis_down = 0
    vis_left = 0
    vis_right = 0
    tree2check = trees[x, y]
    # check up
    for x_pos in range(x-1, -1, -1):
        vis_up += 1
        if trees[x_pos, y] >= tree2check:
            break
    # check down
    for x_pos in range(x+1, trees.shape[0]):
        vis_down += 1
        if trees[x_pos, y] >= tree2check:
            break
    # check left
    for y_pos in range(y-1, -1, -1):
        vis_left += 1
        if trees[x, y_pos] >= tree2check:
            break
    # check right
    for y_pos in range(y+1, trees.shape[1]):
        vis_right += 1
        if trees[x, y_pos] >= tree2check:
            break
    # print(vis_up, vis_down, vis_left, vis_right)
    return vis_up * vis_down * vis_left * vis_right


def max_tree_val(trees):
    max_tree = 0
    for x in range(trees.shape[0]):
        for y in range(trees.shape[1]):
            val = tree_val(x, y, trees)
            if val > max_tree:
                # print("tree at position (%d,%d) has value %d" % (x, y, val))
                max_tree = val
    return max_tree


if __name__ == "__main__":
    file_name = 'input.txt'
    # file_name = 'test_data.txt'
    trees = np.array([[int(tree) for tree in line] for line in open(file_name).read().split("\n")])
    print("The max value of all trees is: ", max_tree_val(trees))
